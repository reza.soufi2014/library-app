import React, {useState} from 'react';
import {BookItem} from "./BookItem";
import {useSelector} from "react-redux";
import Link from "next/link";
import { GoSearch } from "react-icons/go";
import styles from "./styles.module.css";

const BookList = () => {
    let books = useSelector(state => state.app);
    const [ search , setSearch ] = useState('');

    return (
        <div>
            <div className="card px-3">
                <div className="row">
                    <div className="col-lg-6 mt-3">
                        <div className="input-group mb-3">
                            <span className="input-group-text"> <GoSearch /> </span>
                            <input type="text" className={`form-control ${styles.search_input}`} aria-label="Amount (to the nearest dollar)"
                            placeholder="نام کتاب مورد نظر را جستجو کنید" value={search} onChange={e => setSearch(e.target.value)}  />
                        </div>
                    </div>
                    <div className="col-lg-6 d-flex justify-content-center mt-3">
                        <Link href="/add-book">
                            <a>
                                <button className={`btn btn-outline-primary ${styles.add_btn}`}> +  افزودن کتاب </button>
                            </a>
                        </Link>
                    </div>
                </div>
            </div>
            {books.length > 0 ? (
                <div className="row">
                    {
                        books.filter((item) => {
                            if(search === ""){
                                return item
                            }else if(item.title.includes(search)){
                                return item
                            }
                        }).map(book => <BookItem key={book.id} book={book} />)
                    }
                </div>
            ):(
                <div> هیچ کتابی موجود نمی باشد </div>
            )}

        </div>
    );
};

export {BookList};
