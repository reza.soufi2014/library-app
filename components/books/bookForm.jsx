import React, {useState} from 'react';
import {BsFillBookFill, BsCalendarDate} from "react-icons/bs";
import {GiPencilBrush} from "react-icons/gi";
import {MdStarRate} from "react-icons/md";
import {useDispatch} from "react-redux";
import {addBook} from "../../redux/app.actions";
import {useRouter} from "next/router";
import s from "./styles.module.css";
import { DatePicker } from "jalali-react-datepicker";

export const BookForm = () => {
    const [author , setAuthor] = useState('');
    const [title, setTitle] = useState('');
    const [date , setDate] = useState('');
    const [rate , setRate ] = useState('')
    const dispatch = useDispatch();
    const router = useRouter();

    const handleAddBook = e => {
        e.preventDefault();
        if (title !== '' && author !== '' && rate !== '') {
            dispatch(addBook({
                id: Math.random(),
                title,
                author,
                rate
            }));
            setTitle('');
            setAuthor('');
            setRate('');
            router.push("/");
        }
    }

    return (
        <div className="container mt-5">
            <div className={`card card-body mb-4 ${s.title_card}`}>
                <h2 className={s.title_text}> کتاب مورد نظر خود را اضافه کنید </h2>
            </div>
            <div className="card card-body">
                <form onSubmit={handleAddBook}>
                    <div className="row">
                        <div className="col-lg-6 mb-3">
                            <div className="input-group">
                                <div className="input-group-text bg-primary text-white"><BsFillBookFill /></div>
                                <input className="form-control" type="text" value={title} placeholder="نام کتاب را وارد کنید"
                                       onChange={e => setTitle(e.target.value)}/>
                            </div>
                        </div>
                        <div className="col-lg-6 mb-3">
                            <div className="input-group">
                                <div className="input-group-text bg-primary text-white"><GiPencilBrush /></div>
                                <input className="form-control" type="text" value={author} placeholder="نام نویسنده را وارد کنید"
                                       onChange={e => setAuthor(e.target.value)}/>
                            </div>
                        </div>
                        <div className="col-lg-6 mb-3">
                            <div className="input-group">
                                <div className="input-group-text bg-primary text-white"><BsCalendarDate /></div>
                                <DatePicker label=" تاریخ انتشار کتاب" onChange={(date) => setDate(date)} className="form-control" />
                            </div>
                        </div>
                        <div className="col-lg-6 mb-3">
                            <div className="input-group">
                                <div className="input-group-text bg-primary text-white"><MdStarRate /></div>
                                <select value={rate} onChange={e => setRate(e.target.value)} className="form-control">
                                    <option value=""> امتیاز کتاب </option>
                                    <option value="1"> 1 </option>
                                    <option value="2"> 2 </option>
                                    <option value="3"> 3 </option>
                                    <option value="4"> 4 </option>
                                    <option value="5"> 5 </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <button type="submit" className="btn btn-outline-success btn-block mt-4 w-100"> اضافه کردن کتاب
                    </button>
                </form>
            </div>
        </div>
    );
};
