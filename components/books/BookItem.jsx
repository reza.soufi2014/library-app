import React, {useEffect, useState} from 'react';
import { deleteBook} from "../../redux/app.actions";
import { useDispatch } from "react-redux";
import { BsTrash } from "react-icons/bs";
import { Popover, Button } from 'antd';
import Image from "next/image";

const BookItem = (props) => {
    const { id , title , author , rate } = props.book;
    const dispatch = useDispatch();
    const [ visible , setVisible ] = useState(false)

    const hide = () => {
        setVisible(false)
    };

    const handleVisibleChange = visible => {
        setVisible(visible)
    };
    const content = (
        <div className="d-flex justify-content-between">
            <button className="btn btn-danger" onClick={() => dispatch(deleteBook({id : id}))} danger> حذف </button>
            <button className="btn btn-info" onClick={hide}> بستن  </button>
        </div>
    );

    return (
        <>



          <div className="col-lg-4 mt-3">
              <div className="card">
                  <Image src="/img/book.jpg" width={200} height={200} />
                  <div className="card-body">
                      <h5 className="card-title"> نام کتاب : { title }</h5>
                      <p className="card-text">  نویسنده : {author}</p>
                  </div>
                  <ul className="list-group list-group-flush">
                      <li className="list-group-item"> امتیاز کتاب : {rate} از  5  </li>
                      <li className="list-group-item">  تاریخ انتشار کتاب : </li>
                  </ul>
                  <div className="card-body">
                      <Popover
                          content={content}
                          title="از حذف کتاب مطمئنید؟"
                          trigger="click"
                          visible={visible}
                          onVisibleChange={handleVisibleChange}
                      >
                          <Button type="danger"> <BsTrash /> </Button>
                      </Popover>
                  </div>
              </div>
          </div>
        </>
    );
};

export {BookItem};
