import React from 'react';
import {BookForm} from "../components/books/bookForm";
import Head from "next/head";

const AddBook = () => {
    return (
        <>
            <Head>
                <title> افزودن کتاب </title>
            </Head>
            <BookForm />
        </>
    );
};

export default AddBook;
