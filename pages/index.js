import Head from 'next/head';
import {BookList} from "../components/books/BookList";

export default function Home() {
  return (
    <>
        <Head>
            <title> library app </title>
        </Head>
        <div className="container mt-3 pb-5">
            <BookList />
        </div>
    </>
  )
}
