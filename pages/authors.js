import React from 'react';
import {AuthorList} from "../components/authors/AuthorList";

const Authors = () => {
    return (
        <>
            <AuthorList />
        </>
    );
};

export default Authors;
