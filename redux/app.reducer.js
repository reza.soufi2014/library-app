import books from "./data";

export const appReducer = ( state = books , action ) => {
    let newBooks;
    switch (action.type){
        case "ADD_BOOK_SAGA" :
            newBooks = [...state];
            newBooks.push(action.payload);
            return newBooks;
        case "DELETE_BOOK_SAGA" :
            newBooks = [...state];
            newBooks = newBooks.filter(book => book.id !== action.payload)
            return newBooks;

        default:
            return state;
    }
}