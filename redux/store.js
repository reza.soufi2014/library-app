import {createStore, applyMiddleware, compose} from "redux";
import createSagaMiddleware from "redux-saga";
import logger from "redux-logger";
import {rootReducer} from "./root-reducer";
import {books} from "./app.sagas";


const sagaMiddleware = createSagaMiddleware();
const middlewares = [logger , sagaMiddleware];
const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

export const store = createStore( rootReducer , composeEnhancers(applyMiddleware(...middlewares)) );

sagaMiddleware.run(books);