import { put, takeLatest , all , call } from "redux-saga/effects";
import {deleteBookSaga, addBookSaga} from "./app.actions";

export function* onAddBookSaga({payload}){
    yield put(addBookSaga(payload));
}

export function* onDeleteBookSaga({payload: {id} }){
    yield put(deleteBookSaga(id));
}

export function* onDelete(){
    yield takeLatest("DELETE_BOOK" , onDeleteBookSaga);
}
export function* onAdd(){
    yield takeLatest("ADD_BOOK" , onAddBookSaga);
}

export function* books() {
    yield all([call(onDelete) , call(onAdd)]);
}