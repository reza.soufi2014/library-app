export const addBook = (book) => ({
    type : "ADD_BOOK",
    payload : book
});

export const deleteBook = (id) => ({
    type : "DELETE_BOOK",
    payload : id
});

export const updateBook = (book) => ({
    type : "UPDATE_BOOK",
    payload : book
});

export const addBookSaga = (book) => ({
    type : "ADD_BOOK_SAGA",
    payload : book
});

export const deleteBookSaga = (book) => ({
    type : "DELETE_BOOK_SAGA",
    payload : book
});

export const updateBookSaga = (book) => ({
    type : "UPDATE_BOOK_SAGA",
    payload : book
});